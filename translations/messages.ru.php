<?php

return [
    'Amount' => 'Сумма',
    'Credit' => 'Кредит',
    'Term' => 'Срок',
    'Interest rate' => 'Процентная ставка',
    'First payment' => 'Первый платёж',
    'Calculate' => 'Посчитать',
    'Payment date' => 'Дата платежа',
    'Debt' => 'Основной долг',
    'Percent pay' => 'Платеж по процентам',
    'Credit pay' => 'Платеж по кредиту',
    'Annuity payment' => 'Аннуитетный платеж',
    'Overpay' => 'Переплата',
    'Total payment' => 'Общая выплата'
];