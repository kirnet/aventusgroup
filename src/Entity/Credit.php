<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\CreditRepository")
 */
class Credit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $amount;

    /**
     * @ORM\Column(type="integer")
     */
    private $term;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $interest_rate;

    /**
     * @ORM\Column(type="date")
     */
    private $first_payment;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param $amount
     *
     * @return Credit
     */
    public function setAmount($amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getTerm(): ?int
    {
        return $this->term;
    }

    /**
     * @param int $term
     *
     * @return Credit
     */
    public function setTerm(int $term): self
    {
        $this->term = $term;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterestRate()
    {
        return $this->interest_rate;
    }

    /**
     * @param $interest_rate
     *
     * @return Credit
     */
    public function setInterestRate($interest_rate): self
    {
        $this->interest_rate = $interest_rate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getFirstPayment(): ?\DateTimeInterface
    {
        return $this->first_payment;
    }

    /**
     * @param \DateTimeInterface $first_payment
     *
     * @return Credit
     */
    public function setFirstPayment(\DateTimeInterface $first_payment): self
    {
        $this->first_payment = $first_payment;

        return $this;
    }
}
