<?php

namespace App\Controller;

use App\Entity\Credit;
use App\Form\CreditType;
use App\Utils\CreditCalculate;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CreditController
 * @package App\Controller
 */
class CreditController extends AbstractController
{
    /**
     * @Route("/", name="default")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $credit = new Credit();
        $credit->setFirstPayment(new \DateTime('now'));
        $form = $this->createForm(CreditType::class, $credit);
        if ($request->isXmlHttpRequest()) {
            $response = [];
            $jsonResponse = new JsonResponse();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($credit);
                $em->flush();
                $credit = CreditCalculate::calc($credit);
                $response['error'] = false;
                $response['html'] = $this->renderView('credit/table.html.twig', [
                    'credit' => $credit
                ]);
            } else {
                $response['error'] = true;
                $response['html'] = $this->renderView('credit/_form.html.twig', [
                    'form' => $form->createView(),
                ]);
            }
            return $jsonResponse->setData($response);
        }

        return $this->render('credit/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
