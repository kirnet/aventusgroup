<?php

namespace App\Utils;

use App\Entity\Credit;

class CreditCalculate
{
    /**
     * @param $credit
     *
     * @return array
     */
    public static function calc(Credit $credit)
    {
        $round = 2;
        $term = $credit->getTerm();
        $debt = $amount = $credit->getAmount();
        $monthRate = $credit->getInterestRate() / 100 / 12;
        $ratio = ($monthRate * pow((1 + $monthRate), $term)) / ( pow((1 + $monthRate), $term) - 1  );
        $payment = round($ratio * $amount, $round);
        $overpay = ($payment * $term) - $amount;
        $totalPayment = $overpay + $amount;
        $month = $credit->getFirstPayment()->format('m');
        $day =  $credit->getFirstPayment()->format('d');
        $year = $credit->getFirstPayment()->format('Y');
        $schedule = [];

        for ($i = 1; $i <= $term; $i++) {
            $paymentDate = date('d.m.Y', strtotime(sprintf('%d-%02d-%02d', $year,$month, $day) . ' + 1 month'));
            $schedule[$i] = [];
            $percentPay = round($debt * $monthRate, $round) ;
            $creditPay =  round($payment - $percentPay, $round) ;
            $schedule[$i]['payment_date'] = $paymentDate;
            $schedule[$i]['debt'] = number_format($debt, $round, ',', ' ') ;
            $schedule[$i]['percent_pay'] = number_format($percentPay, $round, ',', ' ') ;
            $schedule[$i]['credit_pay'] = number_format($creditPay, $round, ',', ' ') ;
            $schedule[$i]['payment'] =  number_format($payment, $round, ',', ' ') ;
            $debt = $debt - $creditPay;
            if($month++ >= 12) {
                $month = 1;
                $year++;
            }
        }
        return [
            'schedule' => $schedule,
            'overpay' => $overpay,
            'totalPayment' => $totalPayment
        ];
    }
}