'use strict';
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)


// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// var $ = require('jquery');

$(function() {
  $(document).on('submit', '#credit_form', function() {
    $.ajax({
      url: '/',
      type: 'post',
      dataType: 'json',
      data: $(this).serialize(),
      success: function(data) {
        console.log(data);
        let htmlBlock = $('#block_form');
        if (data.error === false) {
          $('.d-block').remove();
          $('.is-invalid').removeClass('is-invalid');
          htmlBlock = $('#result');
        }
        htmlBlock.html(data.html);
      }
    });
    return false;
  });
});
